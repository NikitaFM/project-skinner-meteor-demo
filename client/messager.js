messager = (function () {

  var messages = {
    global: {
      "need-trade-link": function () {
        return "For start trade you must provide the trade link in your profile";
      },
      "trade-api-not-found": function () {
        return "Trade APi is not found.";
      },
      "not-authorized": function () {
        return "You are must be authorized to do that.";
      }
    },
    // "Invalid": 1,
    // "Active": 2, // This trade offer has been sent, neither party has acted on it yet.
    // "Accepted": 3, // The trade offer was accepted by the recipient and items were exchanged.
    // "Countered": 4, // The recipient made a counter offer
    // "Expired": 5, // The trade offer was not accepted before the expiration date
    // "Canceled": 6, // The sender cancelled the offer
    // "Declined": 7, // The recipient declined the offer
    // "InvalidItems": 8, // Some of the items in the offer are no longer available (indicated by the missing flag in the output)
    // "PendingConfirmation": 9, // The offer hasn't been sent yet and is awaiting further confirmation
    // "EmailPending": 9, // (Obsolete)
    // "CanceledConfirmation": 10, // Either party canceled the offer via email/mobile confirmation
    // "EmailCanceled": 10 // (Obsolete)
    offer: {
      "created": function () {
        return "Selling proposition created. Wait for create trade offer.";
      },
      1: function (id) {
        return "Something went wrong when trying to send the trade offer.";
      },
      2: function (id) {
        return "Deposit offer " + id + " is sented, check you steam client.";
      },
      3: function (id) {
        return "Deposit offer " + id + " was accepted.";
      },
      6: function (id) {
        return "Deposit offer " + id + " canceled.";
      },
      7: function (id) {
        return "Deposit offer " + id + " declined.";
      },
      "canceled": function (id) {
        return "Time to accept deposit offerr " + id + " is end.";
      }
    },
    buy: {
      "bought": function () {
        return "You bought.";
      },
      "not-available": function () {
        return "Item(s) is not finded.";
      },
      "no-money": function () {
        return "No money to buy this item(s).";
      },
      "something-wrong": function () {
        return "Something is wrong when you buy item(s).";
      }
    },
    payment: {
      "link-builded": function (amount) {
        return "Payment for buy virtual " + amount + "$ by Pyapal is ready. Confirm the payment in Pyapal system.";
      },
      "build-link-error": function (amount) {
        return "Something was wrong when try to build PayPal payment for buy virtyal " + amount + "$.";
      },
      "payment-success": function (firstName, lastName, amount) {
        return "You buy virtual " + amount + "$.";
      },
      "payment-error": function (amount) {
        return "Something was wrong.";
      }
    },
    payout: {
      "build-payout": function (email, amount) {
        return "Request to PayPal payout " + amount + "$ for " + email + " is sended.";
      },
      "success-payout": function (email, amount) {
        return "Request to PayPal payout " + amount + "$ for " + email + " is success.";
      },
      "error-payout": function (email, amount) {
        return "Something was wrong in PayPal payout " + amount + "$ for " + email + ".";
      }
    }
  }

  return function (category, state) {
    try {
      return messages[category][state].apply(null, Array.prototype.slice.call(arguments, 2));
    }
    catch (err) {
      console.error(
        "Check you messages template. Current message can't find:",
        "\nCategory: " + category + ";",
        "\nState: " + state + "."
      );
    }
  };

})();