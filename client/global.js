socket = io(Meteor.settings.public.BotInventoryAPISocket);

Meteor.startup(function () {
  sAlert.config({
    effect: "slide",
    position: "top-right",
    timeout: 5000,
    html: false,
    onRouteClose: true,
    stack: true,
    offset: 0,
    beep: "/beep.mp3"
  });
});

chunk = function (array, length) {
  return _.map(_.groupBy(array, function (element, index) { return Math.floor(index/length); }), function (val, key) { return { column: val } });;
};

Wallet = new Meteor.Collection("Wallet");
Meteor.subscribe("Wallet");

DepositRequest = new Meteor.Collection("DepositRequest");
Meteor.subscribe("DepositRequest");

UserSellItems = new Meteor.Collection("UserSellItems");
Meteor.subscribe("UserSellItems");

UserBoughtItems = new Meteor.Collection("UserBoughtItems");
Meteor.subscribe("UserBoughtItems");