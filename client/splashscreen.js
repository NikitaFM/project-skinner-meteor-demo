Meteor.startup(function () {
  $("#splashscreen-loader").animate({
    opacity: 0
  }, 300, function () {
    $("#splashscreen-style").remove();
    $("#splashscreen-loader").remove();
  });
});