Router.route("/manager", function () {
  this.render("Manager");
});

Router.route("/", function () {
  this.render("User");
  this.render("UserMain", { to: "UserSection" });
});

Router.route("/buy", function () {
  this.render("User");
  this.render("UserBuy", { to: "UserSection" });
});

Router.route("/sell", function () {
  this.render("User");
  this.render("UserSell", { to: "UserSection" });
});

Router.route("/add-funds", function () {
  this.render("User");
  this.render("AddFunds", { to: "UserSection" });
});

Router.route("/cash-out", function () {
  this.render("User");
  this.render("CashOut", { to: "UserSection" });
});

Router.route("/bought", function () {
  this.render("User");
  this.render("UserBought", { to: "UserSection" });
});

Router.route("/profile", function () {
  this.render("User");
  this.render("UserProfile", { to: "UserSection" });
});