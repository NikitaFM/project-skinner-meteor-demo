Template.UserProfile.helpers({
  tradeLink: function () {
    var user = Meteor.user();
    return !!user && user.profile.tradeLink;
  },
  tradeLinkAccepted: function () {
    var user = Meteor.user();
    if (!Session.get("warnTradeLink") && !!user) {
      return user.profile.tradeLink ? true : false;
    }
  },
  warnTradeLink: function () {
    var user = Meteor.user();
    return !!user && Session.get("warnTradeLink");
  }
});

Template.UserProfile.events({
  "input #trade-link": function (event) {
    var tradeLink = event.target.value;
    var match = tradeLink.match(/https?:\/\/(www.)?steamcommunity.com\/tradeoffer\/new\/?\?partner=\d+(&|&amp;)token=([a-zA-Z0-9-_]+)/);
    var user = Meteor.user();
    if (!!user && match) {
      var token = qs.parse(tradeLink).token;
      Meteor.users.update(Meteor.userId(), {
        $set: { "profile.tradeLink": tradeLink,
                "profile.token": token }
      });
      Session.set("warnTradeLink", false);
    }
    else {
      Meteor.users.update(Meteor.userId(), {
        $set: { "profile.tradeLink": "",
                "profile.token": "" }
      });
      Session.set("warnTradeLink", true);
    }
  }
});