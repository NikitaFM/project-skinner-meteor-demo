Session.setDefault("sell.inventory", [ ]);
Session.setDefault("sell.inventory.cells", 0);
Session.setDefault("sell.inventory.rows", 0);
Session.setDefault("sell.inventory.column", 0);
Session.setDefault("sell.inventory.search", "");

Template.UserSellInventory.onCreated(function () {
  Meteor.call("sell.get-user-inventory", function (err, items) {
    Session.set("sell.inventory", items);
  });
});

Template.UserSellInventory.helpers({
  currentColumn: function () {
    return Session.get("sell.inventory.column")+1;
  },
  columns: function () {
    return Session.get("sell.inventory.columns")+1;
  },
  search: function () {
    return Session.get("sell.inventory.search");
  },
  clear: function () {
    return !!Session.get("sell.inventory.search");
  },
  multiple: function () {
    var columns = Session.get("sell.inventory.columns");
    return columns > 0 ? true : false;
  },
  left: function () {
    var column = Session.get("sell.inventory.column");
    var cells = Session.get("sell.inventory.cells");
    return -(column * cells * 150);
  },
  inventory: function () {
    var user = Meteor.user();
    var inventory = Session.get("sell.inventory");
    var search = Session.get("sell.inventory.search");
    if (search) {
      var regex = new RegExp(search, "i");
      inventory = _.filter(inventory, function (item) {
        return regex.test(item.market_name);
      });
    //   var Inventory = UserItems.find({
    //     state: 0
    //   }).fetch();
    }
    // else {
    //   var regex = new RegExp(search, "i");
    //   var Inventory = UserItems.find({
    //     state: 0,
    //     market_name: {
    //       $regex: regex
    //     }
    //   }).fetch();
    // }
    var pricingItem = Session.get("sell.pricing-item");
    if (pricingItem) {
      inventory = _.filter(inventory, function (item) {
        return !(pricingItem.instanceid === item.instanceid && pricingItem.classid === item.classid);
      });
    }
    var depositItems = Session.get("sell.deposit-items");
    if (depositItems.length > 0) {
      var inventory = _.filter(inventory, function (userItem) {
        var contain = true;
        _.forEach(depositItems, function (item) {
          if (userItem.instanceid === item.instanceid && userItem.classid === item.classid) {
            contain = false;
          }
        });
        return contain;
      });
    }
    ChunkedInventory = chunk(inventory, Session.get("sell.inventory.cells") * Session.get("sell.inventory.rows"));
    var columns = ChunkedInventory.length ? ChunkedInventory.length-1 : 0;
    Session.set("sell.inventory.columns", columns);
    var column = Session.get("sell.inventory.column");
    if (column > columns) Session.set("sell.inventory.column", columns);
    return ChunkedInventory;
  }
});

Template.UserSellInventory.events({
  "click #prev": function () {
    var column = Session.get("sell.inventory.column");
    var columns = Session.get("sell.inventory.columns");
    if (column > 0) column--;
    else if (column === 0) column = columns;
    Session.set("sell.inventory.column", column);
  },
  "click #next": function () {
    var column = Session.get("sell.inventory.column");
    var columns = Session.get("sell.inventory.columns");
    if (column < columns) column++;
    else if (column === columns) column = 0;
    Session.set("sell.inventory.column", column);
  },
  "click #refresh": function () {
    Meteor.call("sell.get-user-inventory", function (err, items) {
      Session.set("sell.inventory", items);
    });
  },
  // "click #search": function (event) {
  //   $(event.target).select()
  // },
  "input #search": function (event) {
    Session.set("sell.inventory.search", event.target.value);
  },
  "click #clear": function (event) {
    Session.set("sell.inventory.search", "");
  },
  "click #item": function () {
    Session.set("sell.pricing-item", this);
    // var inventory = Session.get("sell.inventory");
    // var itemIndex = _.findIndex(inventory, function (item) {
    //   return this.instanceid === item.instanceid && this.classid === item.classid;
    // }, this);
    // var item = inventory.splice(itemIndex, 1);
    // Session.set("sell.inventory", inventory);
    // Session.set("sell.pricing-item", item);
    // console.log(inventory, this, inventory.indexOf(this));
    // var pricedItems = UserItems.find({ state: 1 });
    // if (pricedItems.count() > 0) {
    //   pricedItems.forEach(function (item) {
    //     UserItems.update({ _id: item._id }, { $set: { state: 0 } });
    //   });
    // }
    // UserItems.update({ _id: this._id }, { $set: { state: 1 } });
  }
});

var minHeight = 400;
var rowHeight = 150;
$(window).bind("resize", function (event) {
  var width = $(event.currentTarget).width();
  var height = $(event.currentTarget).height();
  if (1400 >= width) {
    Session.set("sell.inventory.cells", 2);
  }
  else if (width > 1400 && 1800 >= width) {
    Session.set("sell.inventory.cells", 3);
  }
  else if (width > 1800) {
    Session.set("sell.inventory.cells", 4);
  }
  if (minHeight+rowHeight*2 >= height ) {
    Session.set("sell.inventory.rows", 2);
  }
  else if (height > minHeight+rowHeight*2 && minHeight+rowHeight*3 >= height) {
    Session.set("sell.inventory.rows", 3);
  }
  else if (height > minHeight+rowHeight*3 && minHeight+rowHeight*4 >= height) {
    Session.set("sell.inventory.rows", 4);
  }
  else if (height > minHeight+rowHeight*4 && minHeight+rowHeight*5 >= height) {
    Session.set("sell.inventory.rows", 5);
  }
  else if (height > minHeight+rowHeight*5 && minHeight+rowHeight*6 >= height) {
    Session.set("sell.inventory.rows", 6);
  }
  else if (height > minHeight+rowHeight*6) {
    Session.set("sell.inventory.rows", 7);
  }
});
$(window).trigger("resize");

Template.UserSellInventory.onRendered(function () {
  $(".arrow-container," +
    ".pagination-container," +
    ".label"
  ).each(function (i, element) {
    element._uihooks = {
      insertElement: function (node, next) {
        var $node = $(node);
        TweenMax.fromTo($node, 0.15, {
          opacity: 0
        }, {
          opacity: 1,
          ease: Power1.easeOut,
          onStart: function () {
            $node.insertBefore(next);
          }
        });
      },
      removeElement: function(node) {
        var $node = $(node);
        TweenMax.to($node, 0.15, {
          opacity: 0,
          ease: Power1.easeOut,
          onComplete: function () {
            $node.remove();
          }
        });
      }
    };
  });
});