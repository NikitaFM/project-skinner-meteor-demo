Session.setDefault("sell.pricing-item", null);

Template.UserSellPricingItem.helpers({
  item: function () {
    return Session.get("sell.pricing-item");
  },
  exist: function () {
    return !!Session.get("sell.pricing-item");
  },
  price: function () {
    return !Session.get("sell.pricing-item");
  },
  cost: function () {
    var item = Session.get("sell.pricing-item");
    if (!!item && item.price) return item.price.cost;
  },
  our: function () {
    var item = Session.get("sell.pricing-item");
    if (!!item && item.price) return item.price.our+"$";
  },
  user: function () {
    var item = Session.get("sell.pricing-item");
    if (!!item && item.price) return item.price.user+"$";
  },
  deposit: function () {
    var item = Session.get("sell.pricing-item");
    return !(!!item && item.price && item.price.cost > 0);
  }
});

Template.UserSellPricingItem.events({
  "click #cancel": function () {
    Session.set("sell.pricing-item", null);
    // UserItems.update({ _id: item._id }, { $set: { state: 0 } });
  },
  "focus #cost": function (event) {
    $(event.target).select()
  },
  "input #cost": function () {
    var item = Session.get("sell.pricing-item");
    var cost = event.target.valueAsNumber;
    var our = (cost / 10).toFixed(2) * 1;
    var user = (cost - our).toFixed(2) * 1;
    item.price = {
      cost: cost,
      our: our,
      user: user
    };
    Session.set("sell.pricing-item", item);
    // UserItems.update({ _id: item._id }, {
    //   $set: {
    //     "price.cost": cost,
    //     "price.our": our,
    //     "price.user": user
    //   }
    // });
  },
  "click #deposit": function () {
    var item = Session.get("sell.pricing-item");
    if (!!item && item.price && item.price.cost > 0) {
      var depositItems = Session.get("sell.deposit-items");
      depositItems.push(item);
      Session.set("sell.pricing-item", null);
      Session.set("sell.deposit-items", depositItems);
      // UserItems.update({ _id: item._id }, { $set: { state: 2 } });
    }
  }
});

Template.UserSellPricingItem.onRendered(function () {
  this.find(".item-container")._uihooks = {
    insertElement: function (node, next) {
      var $node = $(node);
      TweenMax.fromTo($node, 0.15, {
        opacity: 0
      }, {
        opacity: 1,
        ease: Power1.easeOut,
        onStart: function () {
          $node.insertBefore(next);
        }
      });
    },
    removeElement: function(node) {
      var $node = $(node);
      TweenMax.to($node, 0.15, {
        opacity: 0,
        ease: Power1.easeOut,
        onComplete: function () {
          $node.remove();
        }
      });
    }
  };
});