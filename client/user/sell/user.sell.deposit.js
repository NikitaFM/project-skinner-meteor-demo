Session.setDefault("sell.deposit-items", [ ]);
Session.setDefault("sell.deposit.cells", 0);
Session.setDefault("sell.deposit.rows", 0);
Session.setDefault("sell.deposit.column", 0);
Session.setDefault("sell.deposit.confirm", false);

Template.UserSellDeposit.helpers({
  currentColumn: function () {
    return Session.get("sell.deposit.column")+1;
  },
  columns: function () {
    return Session.get("sell.deposit.columns")+1;
  },
  multiple: function () {
    var columns = Session.get("sell.deposit.columns");
    return columns > 0 ? true : false;
  },
  left: function () {
    var column = Session.get("sell.deposit.column");
    var cells = Session.get("sell.deposit.cells");
    return -(column * cells * 150);
  },
  Deposit: function () {
    var Deposit = Session.get("sell.deposit-items");
    ChunkedDeposit = chunk(Deposit, Session.get("sell.deposit.cells") * Session.get("sell.deposit.rows"));
    var columns = ChunkedDeposit.length ? ChunkedDeposit.length-1 : 0;
    Session.set("sell.deposit.columns", columns);
    var column = Session.get("sell.deposit.column");
    if (column > columns) Session.set("sell.deposit.column", columns);
    return ChunkedDeposit;
  },
  confirm: function () {
    return !(Session.get("sell.deposit-items").length > 0);
  },
  confirmed: function () {
    return !!Session.get("sell.deposit.confirm");
  },
  deposit: function () {
    return !((Session.get("sell.deposit-items").length > 0) && Session.get("sell.deposit.confirm"));
  }
});

Template.UserSellDeposit.events({
  "click #prev": function () {
    var column = Session.get("sell.deposit.column");
    var columns = Session.get("sell.deposit.columns");
    if (column > 0) column--;
    else if (column === 0) column = columns;
    Session.set("sell.deposit.column", column);
  },
  "click #next": function () {
    var column = Session.get("sell.deposit.column");
    var columns = Session.get("sell.deposit.columns");
    if (column < columns) column++;
    else if (column === columns) column = 0;
    Session.set("sell.deposit.column", column);
  },
  "click #remove": function () {
    var deposit = Session.get("sell.deposit-items");
    var itemIndex = _.findIndex(deposit, function (item) {
      return this.instanceid === item.instanceid && this.classid === item.classid;
    }, this);
    var item = deposit.splice(itemIndex, 1);
    Session.set("sell.deposit-items", deposit);
  },
  "change #confirm": function (event) {
    Session.set("sell.deposit.confirm", event.target.checked);
  },
  "click #deposit": function (event) {
    var user = Meteor.user();
    if (!!user.profile.token) {
      // var items = deposit.map(function (item) {
      //   return _.pick(item, "id", "appid", "contextid");
      // });
      var items = Session.get("sell.deposit-items");
      var deposit = {
        userId: user._id,
        steamId: user.profile.id,
        items: items
      };
      sAlert.success(messager("offer", "created"));
      Meteor.call("sell.deposit-items", items, function (err, trade) {
        if (trade) {
          switch (trade.state) {
            case 1:
              sAlert.error(messager("offer", trade.state, trade.id));
              break;
            case 2:
              DepositRequest.insert(deposit);
              sAlert.success(messager("offer", trade.state, trade.id));
              break;
            default:
              console.log("sell.deposit-items", trade);
          }
        }
        else {
          sAlert.error(messager("global", "trade-api-not-found"));
        }
      });
    }
    else {
      sAlert.error(messager("global", "need-trade-link"), { onRouteClose: false });
      Router.go("/profile");
    }
  }
});

Meteor.startup(function () {
  socket.on("deposit.success", function (trade) {
    sAlert.success(messager("offer", trade.state, trade.id));
    Meteor.call("sell.get-user-inventory", function (err, items) {
      Session.set("sell.inventory", items);
      Session.set("sell.deposit-items", [ ]);
    });
  });
  socket.on("sell.sentOfferCanceled", function (trade) {
    sAlert.warning(messager("offer", "canceled", trade.id));
  });
});

Template.UserSellDeposit.onRendered(function () {
  $(".arrow-container," +
    ".pagination-container"
  ).each(function (i, element) {
    element._uihooks = {
      insertElement: function (node, next) {
        var $node = $(node);
        TweenMax.fromTo($node, 0.15, {
          opacity: 0
        }, {
          opacity: 1,
          ease: Power1.easeOut,
          onStart: function () {
            $node.insertBefore(next);
          }
        });
      },
      removeElement: function(node) {
        var $node = $(node);
        TweenMax.to($node, 0.15, {
          opacity: 0,
          ease: Power1.easeOut,
          onComplete: function () {
            $node.remove();
          }
        });
      }
    };
  });
});

var minHeight = 400;
var rowHeight = 150;
$(window).bind("resize", function (event) {
  var width = $(event.currentTarget).width();
  var height = $(event.currentTarget).height();
  if (1400 >= width) {
    Session.set("sell.deposit.cells", 2);
  }
  else if (width > 1400 && 1800 >= width) {
    Session.set("sell.deposit.cells", 3);
  }
  else if (width > 1800) {
    Session.set("sell.deposit.cells", 4);
  }
  if (minHeight+rowHeight*2 >= height ) {
    Session.set("sell.deposit.rows", 2);
  }
  else if (height > minHeight+rowHeight*2 && minHeight+rowHeight*3 >= height) {
    Session.set("sell.deposit.rows", 3);
  }
  else if (height > minHeight+rowHeight*3 && minHeight+rowHeight*4 >= height) {
    Session.set("sell.deposit.rows", 4);
  }
  else if (height > minHeight+rowHeight*4 && minHeight+rowHeight*5 >= height) {
    Session.set("sell.deposit.rows", 5);
  }
  else if (height > minHeight+rowHeight*5 && minHeight+rowHeight*6 >= height) {
    Session.set("sell.deposit.rows", 6);
  }
  else if (height > minHeight+rowHeight*6) {
    Session.set("sell.deposit.rows", 7);
  }
});
$(window).trigger("resize");