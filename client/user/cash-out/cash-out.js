Session.setDefault("payout.amount", 5);
Session.setDefault("payout.is-handled", false);

Template.CashOut.helpers({
  "payoutEmail": function () {
    var user = Meteor.user();
    if (!!user && !!user.profile && !!user.profile.paypalEmail) {
      return user.profile.paypalEmail;
    }
  },
  "payoutAmount": function () {
    return Session.get("payout.amount");
  },
  "disabled": function () {
    var isDisabled = Session.get("payout.is-handled");
    var amount = Session.get("payout.amount");
    var wallet = Wallet.findOne({ });
    var userCash = wallet.cash;
    var isPossible = (parseFloat(amount) + 0.25).toFixed(2) > parseFloat(userCash);
    return isDisabled || isPossible;
  }
});

Template.CashOut.events({
  "input #payout-email": function (event) {
    var value = event.target.value;
    Meteor.users.update(Meteor.userId(), {
      $set: { "profile.paypalEmail": value }
    });
  },
  "input #payout-amount": function (event) {
    var value = event.target.value;
    Session.set("payout.amount", value);
  },
	"submit form[name=\"cash-out\"]": function () {
    var user = Meteor.user();
    var payoutAmount = Session.get("payout.amount");
    Session.set("payout.is-handled", true);
    sAlert.success(messager("payout", "build-payout", user.profile.paypalEmail, payoutAmount));
    Meteor.call("virtyal-currency.paypal.build-payout", {
      receiver: user.profile.paypalEmail,
      amount: payoutAmount
    }, function (err, result) {
      if (err || result.status === 0) {
        sAlert.error(messager("payout", "error-payout", user.profile.paypalEmail, payoutAmount));
      }
      else {
        sAlert.success(messager("payout", "success-payout", user.profile.paypalEmail, payoutAmount));
      }
      Session.set("payout.is-handled", false);
    });
    return false;
  }
});