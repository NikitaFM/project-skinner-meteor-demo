Session.setDefault("add-funds.amount", 0);
Session.setDefault("add-funds.total-amount", [ ]);

var preciseAmount = [
  {
    title: "10$",
    count: 10
  },
  {
    title: "25$",
    count: 25
  },
  {
    title: "50$",
    count: 50
  },
  {
    title: "100$",
    count: 100
  },
  {
    title: "250$",
    count: 250
  },
  {
    title: "500$",
    count: 500
  }
];

Template.AddFunds.helpers({
  "amount": function () {
    var totalAmount = Session.get("add-funds.total-amount");
    var total = 0;
    if (totalAmount.length > 0) {
      Session.set("add-funds.amount", totalAmount.reduce(function (current, sum) {
        return sum + current;
      }));
    }
    else {
      Session.set("add-funds.amount", 0);
    }
    return Session.get("add-funds.amount");
  },
  "preciseAmount": function () {
    return preciseAmount;
  },
  "isCanBeUndo": function () {
    return !(Session.get("add-funds.total-amount").length > 0);
  },
  "isCanBeClear": function () {
    return !(Session.get("add-funds.amount") > 0);
  },
  "isPossible": function () {
    return !(Session.get("add-funds.amount") > 0);
  }
});

Template.AddFunds.events({
  "focus form[name=\"add-funds\"] input[name=\"amount\"]": function (event) {
    $(event.target).select();
  },
  "submit form[name=\"add-funds\"]": function () {
    return false;
  },
  "click button[name=\"precise-amount\"]": function () {
    var totalAmount = Session.get("add-funds.total-amount");
    totalAmount.push(this.count);
    Session.set("add-funds.total-amount", totalAmount);
  },
  "click button[name=\"clear-amount\"]": function () {
    var totalAmount = Session.get("add-funds.total-amount");
    totalAmount.length = 0;
    Session.set("add-funds.total-amount", totalAmount);
  },
  "click button[name=\"undo-amount\"]": function () {
    var totalAmount = Session.get("add-funds.total-amount");
    if (totalAmount.length > 0) {
      totalAmount.pop();
      Session.set("add-funds.total-amount", totalAmount);
    }
  },
  "click button[name=\"build-paypal\"]": function () {
    var totalAmount = Session.get("add-funds.amount");
    Meteor.call("virtyal-currency.paypal.build-payment", {
      amount: totalAmount,
      description: "Payment from Project Skinner to buy " + totalAmount + "$ Virtual Currency."
    }, function (err, result) {
      if (err || result.status === 0) {
        sAlert.error(messager("payment", "build-link-error", totalAmount));
      }
      else {
        sAlert.success(messager("payment", "link-builded", result.amount));
        var title = "_target";
        var width = screen.width - (screen.width * 0.1);
        var height = screen.height - (screen.height * 0.3);
        var left = (screen.width / 2) - (width / 2);
        var top = (screen.height / 2) - (height / 2);
        window.open(result.url, title, "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=" + width + ", height=" + height + ", top=" + top + ", left=" + left);
      }
    });
  }
});

Router.route("/paypal/check", function () {
  Meteor.call("virtyal-currency.paypal.ckeck-payment", this.params.query, function (err, result) {
    if (err || result.status === 0) {
      sAlert.error(messager("payment", "payment-success"));
    }
    else {
      sAlert.success(messager("payment", "payment-error", result.firstName, result.lastName, result.amount));
    }
    window.close();
  });
});