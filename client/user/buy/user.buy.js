Session.setDefault("buy.pages", 1);
Session.setDefault("buy.items-in-column", 4);
Session.setDefault("buy.search", null);
Session.setDefault("buy.search.types", [ ]);

Template.UserBuy.helpers({
  search: function () {
    return Session.get("buy.search");
  },
  SellItems: function () {
    var buyPages = Session.get("buy.pages", 1);
    var buyItemsInColumn = Session.get("buy.items-in-column", 4);
    var currentCount = buyPages * buyItemsInColumn;
    var BuyItems = UserSellItems.find({ }, { limit: currentCount }).fetch();
    ChunkedBuy = chunk(BuyItems, buyItemsInColumn);
    return ChunkedBuy;
  },
  shows: function () {
    var buyPages = Session.get("buy.pages", 1);
    var buyItemsInColumn = Session.get("buy.items-in-column", 4);
    var shows = buyPages * buyItemsInColumn;
    var total = UserSellItems.find({ }).count();
    if (total <= shows) {
      return total;
    }
    else {
      return shows;
    }
  },
  total: function () {
    return UserSellItems.find({ }).count();
  },
  more: function () {
    console.log(UserSellItems.find({ }).fetch());
    var buyPages = Session.get("buy.pages");
    var buyItemsInColumn = Session.get("buy.items-in-column");
    var shows = buyPages * buyItemsInColumn;
    var total = UserSellItems.find({ }).count();
    if (total <= shows) {
      return "passive";
    }
  }
});

Template.UserBuy.events({
  "input #buy-search": function (event) {
    Session.set("buy.search", event.target.value);
    Session.set("buy.pages", 1);
    updateBuyItems();
  },
  "click #reset-search": function () {
    Session.set("buy.search", "");
    updateBuyItems();
  },
  "wheel": function (event) {
    var buyPages = Session.get("buy.pages");
    var buyItemsInColumn = Session.get("buy.items-in-column");
    var shows = buyPages * buyItemsInColumn;
    var total = UserSellItems.find({ }).count();
    var originalEvent = event.originalEvent;
    var $window = $(window);
    var scrollTop = Math.round($window.scrollTop());
    var windowHeight = $window.height();
    var documentHeight = $(document).height();
    if (originalEvent.deltaY > 0 && scrollTop === documentHeight - windowHeight && shows < total) {
      Session.set("buy.pages", ++buyPages);
    }
  },
  "click #more": function () {
    var buyPages = Session.get("buy.pages");
    var buyItemsInColumn = Session.get("buy.items-in-column");
    var shows = buyPages * buyItemsInColumn;
    var total = UserSellItems.find({ }).count();
    if (shows < total) {
      Session.set("buy.pages", ++buyPages);
    }
  },
  "click #type-checkbox [name=\"type\"]": function (event) {
    var element = event.target;
    var value = element.value;
    var searchTypes = Session.get("buy.search.types");
    var index = searchTypes.indexOf(value);
    if (element.checked) {
      if (index === -1) {
        searchTypes.push(value);
      }
    }
    else {
      if (index !== -1) {
        searchTypes.splice(index, 1);
      }
    }
    Session.set("buy.search.types", searchTypes);
    updateBuyItems();
  },
  "click #quick-buy": function () {
    var item = _.pick(this, "ownerId", "ownerSteamId", "appid", "contextid", "classid", "instanceid", "cost");
    Meteor.call("buy.quick-buy-item", [
      item
    ], function (err, result) {
      switch (result) {
        case "success":
          sAlert.success(messager("buy", "bought"));
          break;
        case "no-item":
          sAlert.error(messager("buy", "not-available"));
          break;
        case "no-money":
          sAlert.error(messager("buy", "no-money"));
          break;
        default:
          sAlert.error(messager("buy", "something-wrong"));
      }
    });
  }
});