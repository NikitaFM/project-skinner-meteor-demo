Session.setDefault("user-menu", false);

Meteor.startup(function () {
  if (Boolean(Meteor.userId())) {
    Meteor.call("user.get-player-summaries");
  }
});

Template.User.helpers({
  user: function () {
    return Meteor.user();
  }
});

Template.User.helpers({
  "userMenu": function () {
    return Session.get("user-menu") ? "active" : "";
  },
  "menuIcon": function () {
    return Session.get("user-menu") ? "fa-caret-up" : "fa-caret-down";
  },
  "wallet": function () {
    var wallet = Wallet.findOne({ });
    return wallet;
  }
});

Template.User.events({
  "click #steam-login": function () {
    Meteor.loginWithSteam(function (error) {
      if (error) return;
      Meteor.call("user.get-player-summaries");
      Meteor.call("user.create-wallet");
      Meteor.call("sell.get-user-inventory", function (err, items) {
        Session.set("sell.inventory", items);
      });
    });
  },
  "click #menu-activator": function () {
    Session.set("user-menu", !Session.get("user-menu"));
  },
  // "mouseover #menu": function () {
  //   Session.set("user-menu", false);
  // },
  // "blur #menu": function () {
  //   Session.set("user-menu", false);
  // },
  "click #user-logout": function () {
    Meteor.logout();
  }
});

Template.User.onRendered(function () {
  this.find("#main-content")._uihooks = {
    insertElement: function (node, next) {
      var $node = $(node);
      TweenMax.fromTo($node, 0.5, {
        opacity: 0,
        z: 50
      }, {
        opacity: 1,
        z: 0,
        onStart: function () {
          $node.insertBefore(next);
        }
      });
    },
    removeElement: function(node) {
      var $node = $(node);
      TweenMax.set($node, { position: "absolute" });
      TweenMax.to($node, 0.5, {
        opacity: 0,
        z: -50,
        onComplete: function () {
          $node.remove();
        }
      });
    }
  };
});