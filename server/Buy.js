var Future = Npm.require("fibers/future");

Meteor.methods({
  "buy.quick-buy-item": function (items) {
    this.unblock();
    var user = Meteor.user();
    if (!!user) {
      var future = new Future();
      HTTP.get(Meteor.settings.BotInventoryAPI + "/bot-inventory", function (req, res) {
        var botInventory = res.data;
        var wallet = Wallet.findOne({
          ownerId: user._id
        });
        var cash = parseFloat(wallet.cash);
        var costs = 0;
        var error = null;
        var boughtItems = items.map(function (sellItem) {
          var exist = botInventory.some(function (inventoryItem) {
            return sellItem.classid === inventoryItem.classid && sellItem.instanceid === inventoryItem.instanceid;
          });
          if (Boolean(exist)) {
            costs = (parseFloat(costs) + parseFloat(sellItem.cost)).toFixed(2);
            return sellItem;
          }
          else {
            error = true;
          }
        });
        if (boughtItems.length > 0 && !error) {
          if (costs <= cash) {
            Wallet.update(wallet._id, {
              $set: {
                cash: (cash - costs).toFixed(2)
              }
            });
            boughtItems.forEach(function (boughtItem) {
              var sellCost = boughtItem.cost;
              var sellerCash = parseFloat((sellCost * 0.9).toFixed(2));
              console.log(sellerCash);
              Wallet.update({
                ownerId: boughtItem.ownerId
              }, {
                $inc: {
                  cash: sellerCash
                }
              });
              console.log(boughtItem);
              UserSellItems.remove({
                classid: boughtItem.classid,
                instanceid: boughtItem.instanceid
              });
              var owner = {
                ownerId: user._id,
                ownerSteamId: user.profile.id
              };
              userBoughtItem = _.extend(boughtItem, owner);
              UserBoughtItems.insert(userBoughtItem);
            });
            future.return("success");
          }
          else {
            future.return("no-money");
          }
        }
        else {
          future.return("no-item");
        }
      });
      return future.wait();
    }
  }
});