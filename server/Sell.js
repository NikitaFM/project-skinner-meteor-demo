var Future = Npm.require("fibers/future");
var querystring = Meteor.npmRequire("querystring");

UserSellItems = new Meteor.Collection("UserSellItems");

Meteor.publish("UserSellItems", function () {
  return UserSellItems.find({ });
});

UserSellItems.deny({
  insert: function () { return false; },
  update: function () { return false; },
  remove: function () { return false; }
});

Meteor.methods({
  "sell.get-user-inventory": function () {
    this.unblock();
    var user = Meteor.user();
    if (!!user) {
      var steamId = user.profile.id;
      var future = new Future();
      var query = {
        steamId: steamId
      };
      HTTP.get(Meteor.settings.BotInventoryAPI + "/user-inventory?" + querystring.stringify(query), function (req, res) {
        var inventory = res.data;
        future.return(inventory);
      });
      return future.wait();
    }
  },
  "sell.deposit-items": function (items) {
    this.unblock();
    var user = Meteor.user();
    if (!!user) {
      var steamId = user.profile.id;
      var token = user.profile.token;
      var items = _.map(items, function (skin) {
        return _.pick(skin, "id", "appid", "contextid");
      });
      if (steamId && token && items.length > 0) {
        var deposit = {
          sallerId: steamId,
          token: token,
          items: items
        };
        var future = new Future();
        HTTP.get(Meteor.settings.BotInventoryAPI + "/sell" + "?" + qs.stringify(deposit), function (req, res) {
          future.return(res.data);
        });
        return future.wait();
      }
    }
  }
});

Meteor.startup(function () {
  socket.on("deposit.success", Meteor.bindEnvironment(function (trade) {
    var deposits = DepositRequest.find({
      steamId: trade.partnerId
    }).fetch();
    trade.itemsToReceive.forEach(function (receiveItem) {
      var price;
      var ownerId;
      deposits.forEach(function (deposit) {
        deposit.items.forEach(function (item) {
          if (receiveItem.classid === item.classid && receiveItem.assetid === item.assetid) {
            price = item.price;
          }
        });
      });
      var owner = {
        ownerId: trade.partnerId,
        ownerSteamId: trade.partnerId,
        cost: price.cost
      };
      _.extend(receiveItem, owner);
      UserSellItems.insert(receiveItem);
    });
  }));
});