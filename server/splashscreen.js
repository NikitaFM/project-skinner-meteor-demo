Inject.rawHead("loader-style", "" +
  "<meta name=\"viewport\" content=\"width=device-width,maximum-scale=1, initial-scale=1,user-scalable=no\">" +
  "<style>" +
    "html { height: 100%; }" +
    "body { margin: 0; min-height: 100%; }" +
  "</style>" +
  "<style id=\"splashscreen-style\">" +
    ".splashscreen-loader { background-color: #fff; position: fixed; top: 0; left: 0; right: 0; bottom: 0; }" +
    ".centered-container { position: absolute; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%); }" +
  "</style>" +
"");
Inject.rawHead("loader-body2", "<body><div id=\"splashscreen-loader\" class=\"splashscreen-loader\"><div class=\"centered-container\"><div class=\"loader\"></div></div></div></body>");