var querystring = Meteor.npmRequire("querystring");

Meteor.methods({
  "bought.create-get-trade": function () {
    this.unblock();
    var user = Meteor.user();
    if (Boolean(user)) {
      var userId = user._id;
      var userSteamId = user.profile.id;
      var token = user.profile.token;
      var boughtItems = UserBoughtItems.find({
        ownerId: userId,
        ownerSteamId: userSteamId
      }).fetch();
      if (boughtItems.length > 0) {
        var tradeItems = boughtItems.map(function (boughtItem) {
          // console.log("#", boughtItem);
          // console.log("##", _.pick(boughtItem, "appid", "contextid", "classid", "instanceid"));
          return _.pick(boughtItem, "appid", "contextid", "classid", "instanceid");
        });
        // console.log(tradeItems);
        var boughtItemsQuery = {
          ownerToken: token,
          ownerSteamId: userSteamId,
          items: tradeItems
        };
        HTTP.get(Meteor.settings.BotInventoryAPI + "/bought-trade" + "?" + qs.stringify(boughtItemsQuery), function (req, res) {
          
        });
      }
    }
  }
});