var querystring = Meteor.npmRequire("querystring");

Meteor.methods({
  "user.get-player-summaries": function () {
    this.unblock();
    var user = Meteor.user();
    if (Boolean(user) && Boolean(user.profile.id)) {
      HTTP.get(Meteor.settings.BotInventoryAPI + "/user-summaries/" + user.profile.id, function (err, res) {
        if (err) {
          console.log(err);
        }
        else {
          var profile = res.data;
          Meteor.users.update(user._id, {
            $set: {
              "profile.name": profile.name,
              "profile.avatarHash": profile.avatarHash
            }
          });
        }
      });
    }
  },
  "user.create-wallet": function () {
    this.unblock();
    var wallet = Wallet.findOne({
      ownerId: this.userId
    });
    if (!wallet) {
      var newWallet = {
        ownerId: this.userId,
        cash: (0).toFixed(2),
        payments: [ ],
        payouts: [ ]
      };
      Wallet.insert(newWallet);
    }
  }
});