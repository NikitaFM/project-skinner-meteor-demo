var paypal = {
  getToken: function (params, callback) {
    var data = qs.stringify({
      "grant_type": "client_credentials"
    });
    var req = https.request({
      method: "POST",
      hostname: "api.sandbox.paypal.com",
      path: "/v1/oauth2/token",
      headers: {
        "Accept": "application/json",
        "Content-Length": data.length
      },
      auth: params.clientId + ":" + params.secret
    }, function (res) {
      var result = "";
      res.setEncoding("UTF-8");
      res.on("data", function(chunk) {
        result += chunk;
      });
      res.on("end", function () {
        callback(null, JSON.parse(result));
      });
    });
    req.write(data);
    req.end();
  },
  createPayment: function (params, callback) {
    var data = JSON.stringify({
      "intent": "sale",
      "redirect_urls": {
        "return_url": "http://localhost:3000" + "/paypal/check",
        "cancel_url": "http://localhost:3000/add-funds"
      },
      "payer": {
        "payment_method": "paypal"
      },
      "transactions": [
        {
          "amount": {
            "total": params.amount,
            "currency": "USD"
          },
          "description": params.description
        }
      ]
    });
    var req = https.request({
      method: "POST",
      hostname: "api.sandbox.paypal.com",
      path: "/v1/payments/payment",
      headers: {
        "Authorization": "Bearer " + params.token,
        "Content-Type": "application/json",
        "Content-Length": data.length
      }
    }, function (res) {
      var result = "";
      res.setEncoding("UTF-8");
      res.on("data", function(chunk) {
        result += chunk;
      });
      res.on("end", function () {
        try {
          callback(null, JSON.parse(result));
        }
        catch (e) {
          callback(e);
        }
      });
    });
    req.write(data);
    req.end();
  },
  executePayment: function (params, okCallback, notCallback) {
    var data = JSON.stringify({
      "payer_id" : params.PayerID
    });
    var req = https.request({
      method: "POST",
      hostname: "api.sandbox.paypal.com",
      path: "/v1/payments/payment/" + params.paymentId + "/execute",
      headers: {
        "Authorization": "Bearer " + params.token,
        "Content-Type": "application/json",
        "Content-Length": data.length
      }
    }, function (res) {
      var result = "";
      res.setEncoding("UTF-8");
      res.on("data", function(chunk) {
        result += chunk;
      });
      res.on("end", function () {
        try {
          var detail = JSON.parse(result);
          switch (detail.state) {
            case "approved":
              okCallback(detail)
              break;
            default:
              console.log("#1", detail);
              notCallback(detail);
          }
        }
        catch (e) {
          notCallback(detail);
        }
      });
    });
    req.write(data);
    req.end();
  },
  createPayout: function (params, callback) {
    var data = JSON.stringify({
      "sender_batch_header": {
        "email_subject": "Payout from project skinner. Buy " + params.amount + "virtual $."
      },
      "items": [
        {
          "recipient_type": "EMAIL",
          "amount": {
            "value": params.amount,
            "currency": "USD"
          },
          "receiver": params.receiver
        }
      ]
    });
    var req = https.request({
      method: "POST",
      hostname: "api.sandbox.paypal.com",
      path: "/v1/payments/payouts?sync_mode=true",
      headers: {
        "Authorization": "Bearer " + params.token,
        "Content-Type": "application/json",
        "Content-Length": data.length
      }
    }, function (res) {
      var result = "";
      res.setEncoding("UTF-8");
      res.on("data", function(chunk) {
        result += chunk;
      });
      res.on("end", function () {
        callback(null, JSON.parse(result));
      });
    });
    req.write(data);
    req.end();
  }
};

Meteor.methods({
  "virtyal-currency.paypal.build-payment": function (params) {
    this.unblock();
    var user = Meteor.user();
    var message = {
      status: 0,
      amount: null,
      url: null
    };
    if (user) {
      var future = new Future();
      paypal.getToken({
        clientId: Meteor.settings.PayPal.clientId,
        secret: Meteor.settings.PayPal.secret
      }, function (err, token) {
        if (err) {
          future.return(message);
        }
        else {
          paypal.createPayment({
            token: token.access_token,
            amount: params.amount,
            description: params.description
          }, function (err, payment) {
            if (err) {
              future.return(message);
            }
            else {
              var approval_url = _.findWhere(payment.links, {
                rel: "approval_url"
              });
              message.status = 1;
              message.amount = payment.transactions[0].amount.total;
              message.url = approval_url.href;
              future.return(message);
            }
          });
        }
      });
    }
    else {
      future.return(message);
    }
    return future.wait();
  }
});

Meteor.methods({
  "virtyal-currency.paypal.ckeck-payment": function (params) {
    this.unblock();
    var user = Meteor.user();
    var future = new Future();
    var message = {
      status: 0,
      firstName: null,
      lastName: null,
      amount: null
    };
    if (!!user) {
      paypal.getToken({
        clientId: Meteor.settings.PayPal.clientId,
        secret: Meteor.settings.PayPal.secret
      }, Meteor.bindEnvironment(function (err, token) {
        if (err) {
          future.return(message);
        }
        else {
          paypal.executePayment({
            token: token.access_token,
            paymentId: params.paymentId,
            PayerID: params.PayerID
          }, Meteor.bindEnvironment(function (payment) {
            var wallet = Wallet.findOne({
              ownerId: user._id
            });
            var userPayments = wallet.payments;
            var userCash = wallet.cash;
            var isExistUserPayment = !!_.where(userPayments, { id: payment.id }).length;
            if (!isExistUserPayment) {
              var paymentInfo = {
                id: payment.id,
                payerId: payment.payer.payer_info.payer_id,
                firstName: payment.payer.payer_info.first_name,
                lastName: payment.payer.payer_info.last_name,
                createTime: payment.create_time,
                amount: parseFloat(payment.transactions[0].amount.total),
                status: 1
              }
              userPayments.push(paymentInfo);
              userCash = (parseFloat(userCash) + parseFloat(paymentInfo.amount)).toFixed(2);;
              Wallet.update(wallet._id, {
                $set: {
                  cash: userCash,
                  payments: userPayments
                }
              });
              message.status = 1;
              message.amount = paymentInfo.amount;
              message.firstName = paymentInfo.firstName;
              message.lastName = paymentInfo.lastName;
              future.return(message);
            }
            else {
              future.return(message);
            }
          }), Meteor.bindEnvironment(function () {
            console.log("#2");
            future.return(message);
          }));
        }
      }));
    }
    else {
      future.return(message);
    }
    return future.wait();
  }
});

Meteor.methods({
  "virtyal-currency.paypal.build-payout": function (params) {
    this.unblock();
    var user = Meteor.user();
    var future = new Future();
    var message = {
      status: 0
    };
    if (user) {
      paypal.getToken({
        clientId: Meteor.settings.PayPal.clientId,
        secret: Meteor.settings.PayPal.secret
      }, Meteor.bindEnvironment(function (err, token) {
        if (err) {
          future.return(message);
        }
        else {
          var wallet = Wallet.findOne({
            ownerId: user._id
          });
          var cash = parseFloat(wallet.cash);
          var payouts = wallet.payouts;
          if ((parseFloat(params.amount) + 0.25).toFixed(2) <= cash) {
            paypal.createPayout({
              token: token.access_token,
              amount: params.amount,
              receiver: params.receiver
            }, Meteor.bindEnvironment(function (err, payout) {
              if (err) {
                future.return(message);
              }
              else {
                var status = payout.items[0].transaction_status;
                payouts.push(payout);
                if (status === "SUCCESS") {
                  var total = (parseFloat(payout.batch_header.amount.value) + parseFloat(payout.batch_header.fees.value)).toFixed(2);
                  var newCash = (cash - total).toFixed(2);
                  Wallet.update(wallet._id, {
                    $set: {
                      cash: newCash,
                      payouts: payouts
                    }
                  });
                  message.status = 1;
                  future.return(message);
                }
                else {
                  future.return(message);
                }
              }
            }));
          }
          else {
            future.return(message);
          }
        }
      }));
    }
    else {
      future.return(message);
    }
    return future.wait();
  }
});