Future = Npm.require("fibers/future"); // deprecated
qs = Meteor.npmRequire("qs"); // deprecated

var io = Meteor.npmRequire("socket.io-client");
socket = io(Meteor.settings.BotInventoryAPISocket);

Wallet = new Meteor.Collection("Wallet");
Wallet.deny({
  insert: function () { return false; },
  update: function () { return false; },
  remove: function () { return false; }
});
Meteor.publish("Wallet", function () {
  return Wallet.find({
    ownerId: this.userId
  });
});

DepositRequest = new Meteor.Collection("DepositRequest");
Meteor.publish("DepositRequest", function () {
  return DepositRequest.find({
    userId: this.userId
  }, {
    sort: { createdAt: 1 }
  });
});

UserBoughtItems = new Meteor.Collection("UserBoughtItems");
Meteor.publish("UserBoughtItems", function () {
  return UserBoughtItems.find({
    ownerId: this.userId
  });
});
UserBoughtItems.deny({
  insert: function () { return false; },
  update: function () { return false; },
  remove: function () { return false; }
});

// Meteor.publish("SellItems", function () {
//   console.log(this.userId);
//   return SellItems.find({ });
// });

// socket.on("sell.sentOfferChange", Meteor.bindEnvironment(function (trade) {
//   switch (trade.state) {
//     case 3:
//       trade.itemsToReceive.forEach(function (item) {
//         item.ownerId = trade.pertner;
//         UserSellItems.insert({ ownerId: trade.pertner });
//       });
//       break;
//     default:
//       console.log("sell.sentOfferChange", trade);
//   }
// }));

Meteor.startup(function () {
  ServiceConfiguration.configurations.upsert(
    { service: "steam" },
    {
      $set: {
        loginStyle: "popup",
        timeout: 10000
      }
    }
  );
});

https = Npm.require("https");